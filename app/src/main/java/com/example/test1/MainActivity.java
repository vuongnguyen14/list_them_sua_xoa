package com.example.test1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lvMonHoc;
    ArrayList<String> arrayCourse;
    EditText edtMonhoc;
    Button btthem, btsua;

    int vitri = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // ánh xạ
        lvMonHoc = (ListView) findViewById(R.id.listviewMonhoc);
        edtMonhoc= (EditText) findViewById(R.id.editTextMonhoc);
        btthem = (Button) findViewById(R.id.button_them);
        btsua =(Button) findViewById(R.id.buttonsua);

        // thêm phần tử vào mảng
        arrayCourse= new ArrayList<>();
        arrayCourse.add("Java");
        arrayCourse.add("C#");
        arrayCourse.add("Android");
        arrayCourse.add("Php");
        arrayCourse.add("Quản Trị Mạng");

        ArrayAdapter    adapter= new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1,arrayCourse);

        lvMonHoc.setAdapter(adapter);

        // click
        lvMonHoc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                // trả về vị trid click tren listview
                Toast.makeText(MainActivity.this,arrayCourse.get(i), Toast.LENGTH_SHORT).show();
            }
        });

        // nhấn giữ
        lvMonHoc.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int i, long l) {
                arrayCourse.remove(i);
                adapter.notifyDataSetChanged();

                return false;
            }
        });

        btthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String monhoc= edtMonhoc.getText().toString();
                arrayCourse.add(monhoc);
                adapter.notifyDataSetChanged();
            }
        });

        lvMonHoc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                edtMonhoc.setText(arrayCourse.get(i));
                vitri =i ;

            }
        });
        btsua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayCourse.set(vitri,edtMonhoc.getText().toString());
                adapter.notifyDataSetChanged();
            }
        });
    }
}